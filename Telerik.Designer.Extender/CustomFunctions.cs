﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Telerik.Designer.Extender
{
	public static class CustomFunctions
	{
		private static Dictionary<string, string> translations = new Dictionary<string, string>();
		public static bool isTelerikRuntime = false;
		public static string filePath = string.Empty;

		public static string GetLocalizedString(string key)
		{
			string path = string.Format(new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments)).Parent.FullName + "/Resources/");

			File.AppendAllText(@"D:\Hatteland src\Drivers\System.OutputDesign.TelerikRuntime (3324)\TelerikRuntime\bin\Debug\debug.txt", "File path:" + path);
			
			if(isTelerikRuntime && !string.IsNullOrEmpty(filePath))
			{
				path = filePath + "\\";
			}


			FileInfo[] names = new DirectoryInfo(path).GetFiles("*.po");


			if(names == null || names.Length == 0)
			{
				return "";
			}

			FileInfo nameInfo = names.FirstOrDefault();

			if(nameInfo == null)
			{
				return "";
			}

			string name = nameInfo.Name;
			string key1 = string.Empty;

			using(StreamReader streamReader = new StreamReader(path + name))
			{
				string str1;
				while((str1 = streamReader.ReadLine()) != null)
				{
					if(str1.Contains("msgid"))
					{
						key1 = str1.Substring(0).Replace("\"", "").Replace("msgid", "");
						File.AppendAllText(@"D:\Hatteland src\Drivers\System.OutputDesign.TelerikRuntime (3324)\TelerikRuntime\bin\Debug\debug.txt", key1);
					}
					else if(str1.Contains("msgstr"))
					{
						string str2 = str1.Substring(0).Replace("\"", "").Replace("msgstr", "");
						if(!CustomFunctions.translations.ContainsKey(key1))
							CustomFunctions.translations.Add(key1, str2);
					}
				}
			}
			foreach(KeyValuePair<string, string> translation in CustomFunctions.translations)
			{
				if(translation.Key != null && translation.Key.ToString().ToUpper() == " " + key.ToUpper())
				{
					File.AppendAllText(@"D:\Hatteland src\Drivers\System.OutputDesign.TelerikRuntime (3324)\TelerikRuntime\bin\Debug\debug.txt", translation.Key + key.ToUpper());
					return translation.Value.ToString();
				}
			}
			return key;
		}
	}
}
